# TP1- Algoritmos y Estr. de Datos III

Integrantes:
- Luciano Tarsia
- Nicolas Repollo

Fecha de entrega: 4/10/2020

# ¿Cómo usarlo?

Para compilar el código de C++ (main.cpp) se debe abrir la terminal, ubicarse en el directorio del trabajo y correr el siguiente comando:

g++ main.cpp -o main

Luego de ejercutar este comando, se creara el directorio un archivo main.exe. El mismo se puede ejecutar desde la terminal declarando el algoritmo que se desea ejecutar de la siguiente manera:

main [Algoritmo]  (Donde [Algoritmo] es alguno de los siguientes {FB, BT, DP})

