#include <iostream>
#include <vector>
#include <chrono>
#include <map>
#include <algorithm>

using namespace std;

// n: Cantidad de locales, M: Contagio máximo permitido
int n, M;

// C: Vector costo, B: Vector beneficio
vector<int> B, C;

// Variables globales para BT
int beneficio;
int optimo = -1; // (Usada para la poda por optimalidad)

//Defino una variable que representa al infinito. Se usa en casos bordes
int infinito = std::numeric_limits<int>::max();



// Algoritmo de Fuerza Bruta(FB). Se llama con FB(0,C,B,false,false)

int FB(int i, int contagio_acum, int beneficio_acum, bool flag_consecutivos, bool decision_anterior) {
    // flag consecutivos indica si hay/hubo dos locales abiertos consecutivos en la instancia.
    // decisión anterior indica si el local anterior al que estoy parado fue abierto.
    if (i == B.size()) {
        if (contagio_acum <= M and not flag_consecutivos) {
            return beneficio_acum;
        } else {
            return -infinito;
        }
    }
    if (decision_anterior) { // Si abrí el local anterior, decision_anterior es true.
        return max(FB(i + 1, contagio_acum + C[i], beneficio_acum + B[i], true, true),
                   FB(i + 1, contagio_acum, beneficio_acum, flag_consecutivos, false));
    } else {
        return max(FB(i + 1, contagio_acum + C[i], beneficio_acum + B[i], flag_consecutivos, true),
                   FB(i + 1, contagio_acum, beneficio_acum, flag_consecutivos, false));
    }
}



//Algoritmo de Backtracking(BT). Se llama con BT(0,0,0)

int BT_aux(int i, int contagio_acum, int beneficio_acum,const vector<int> &beneficios_max_restantes);

int BT(int i, int contagio_acum, int beneficio_acum) {

    vector<int> beneficios_max_restantes; // (Usado para la poda por optimalidad)
    beneficios_max_restantes.assign(n, 0);
    beneficios_max_restantes[n - 1] = B[n - 1];

    for (int k = 1; k < n; k++) {
        beneficios_max_restantes[n - (k + 1)] += beneficios_max_restantes[n - k] + B[n - (k + 1)];
    }

    return BT_aux(i,contagio_acum,beneficio_acum, beneficios_max_restantes);
}

int BT_aux(int i, int contagio_acum, int beneficio_acum,const vector<int> &beneficios_max_restantes) {

    //Caso base
    if (i >= B.size() and contagio_acum <= M) {
        if(beneficio_acum > optimo){
            optimo = beneficio_acum;
        }
        return beneficio_acum;
    } else if (i >= B.size()) {
        return -infinito;
    }

    // Recursividad

    // Poda por optimalidad
    if (beneficio_acum + beneficios_max_restantes[i] < optimo) {
        return -1;
    }

    // Notar que en el return está la poda por factibilidad no explicita (Poda los negocios consecutivos)
    return max(BT_aux(i + 1, contagio_acum, beneficio_acum, beneficios_max_restantes),
                BT_aux(i + 2, contagio_acum + C[i], beneficio_acum + B[i], beneficios_max_restantes));
}



//Algoritmo de Programación dinámica(PD). Se llama con PD(0,0)

int PD_aux(int i, int contagio_acum, vector<vector<int>> &memo);

int PD(int i, int contagio_acum) {

    vector<vector<int>> memo;
    for (int j = 0; j < B.size(); ++j) {
        vector<int> aux;
        aux.assign(M+1, -1);
        memo.push_back(aux);
    }
    return PD_aux(i, contagio_acum, memo);
}

//Algoritmo auxiliar de PD
int PD_aux(int i, int contagio_acum, vector<vector<int>> &memo) {

    //Caso: Resultado guardado en memoizacion o borde/base
    if (contagio_acum > M) { //Si se pasa del límite de contagio
        return -infinito;
    }
    else if(i >= B.size()) { //Si llega al caso base
        return 0;
    }
    else if(memo[i][contagio_acum] != -1) { //Si está guardado el resultado
        //cout << "i = " << i << ",   " << contagio_acum << ",   " << "EN MEMO" << endl;
        return memo[i][contagio_acum];
    }

    //Caso: Resultado no guardado en memoizacion
    else {

        //Recursividad
        int aux = max(PD_aux(i + 2, contagio_acum + C[i], memo) + B[i], PD_aux(i + 1, contagio_acum, memo));
        memo[i][contagio_acum] = aux;

        return memo[i][contagio_acum];
    }
}


int main(int argc, char **argv) {
    beneficio = -1;
    // Leemos el parametro que indica el algoritmo a ejecutar.
    map<string, string> algoritmos_implementados = {
            {"FB", "Fuerza Bruta"},
            {"BT", "Backtracking con podas"},
            {"DP", "Programacion dinámica"}
    };

    // Verificar que el algoritmo pedido exista.
    if (argc < 2 or algoritmos_implementados.find(argv[1]) == algoritmos_implementados.end()) {
        cerr << "Algoritmo no encontrado: " << argv[1] << endl;
        cerr << "Los algoritmos existentes son: " << endl;
        for (auto &alg_desc: algoritmos_implementados)
            cerr << "\t- " << alg_desc.first << ": " << alg_desc.second << endl;
        return 0;
    }
    string algoritmo = argv[1];

    // Leemos el input.
    cin >> n >> M;
    B.assign(n, 0);
    C.assign(n, 0);
    for (int i = 0; i < n; i++) {
        cin >> B[i];        // Se piden los costos
        cin >> C[i];        // Se piden los beneficios
    }


    // Ejecutamos el algoritmo y obtenemos su tiempo de ejecución.
    auto start = chrono::steady_clock::now();
    if (algoritmo == "FB") {
        beneficio = FB(0, 0, 0, false, false);

    } else if (algoritmo == "BT") {
        beneficio = BT(0, 0, 0);

    } else if (algoritmo == "DP") {
        beneficio = PD(0,0);
    }

    auto end = chrono::steady_clock::now();
    double total_time = chrono::duration<double, milli>(end - start).count();

    // Imprimimos el tiempo de ejecución por stderr.
    clog << total_time << endl;

    // Imprimimos el resultado por stdout.
    cout << beneficio << endl;
    return 0;
}